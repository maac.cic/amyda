---
title: A empresa
subtitle: Quem somos?
comments: false
---

Somos uma empresa que oferece soluções especializadas para o setor de alimentos e bebidas, contribuindo com a qualidade, segurança e sustentabilidade em todos os elos da cadeia de produção.

### Missão

Atuar junto aos produtores e distribuidores de alimentos de forma ética, segura e rentável, fornecendo soluções que atendam às necessidades das empresas, pessoas, e requisitos regulatórios.

### Visão

Ser referência no desenvolvimento e implantação de soluções inovadoras para aqueles que se preocupam com a satisfação e a saúde dos seus clientes.

### Valores

 - Promover a disponibilidade de alimentos seguros para todas as pessoas;
 - Colaborar com a difusão do conhecimento entre os produtores e consumidores.

